/*
    VANILLA JAVASCRIPT
*/
// let rootId = document.getElementById('root');
// let elementH1 = document.createElement('h1');

// elementH1.innerText = 'Hello, React!';
// rootId.append(elementH1);


const element = (
    <nav>
        <h1 className="brand-name">Cisco</h1>
        <ul>
            <li>Pricing</li>
            <li>About</li>
            <li>Contact</li>
        </ul>
    </nav>
)

ReactDOM.render(element, document.getElementById('root'));